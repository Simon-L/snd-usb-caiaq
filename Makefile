# SPDX-License-Identifier: GPL-2.0-only
snd-usb-caiaq-y := device.o audio.o midi.o control.o
snd-usb-caiaq-y += input.o

obj-m += snd-usb-caiaq.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean