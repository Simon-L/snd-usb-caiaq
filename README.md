### snd-usb-caiaq

Using MIDI instead of a Linux input device for the Rig Kontrol 3 buttons etc.

:warning: This has a few breaking changes compared to the code found in the official source tree, do not merge.

* Expose the main device as "RigKontrol3 MIDI" for the hardware ports, "RigKontrol3" for the virtual interface from the switches.
* Switches 1-9 are mapped to CC 64-72, 127 value for on

* How should the leds and screen be mapped?